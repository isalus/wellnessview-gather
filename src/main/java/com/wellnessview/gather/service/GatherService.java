package com.wellnessview.gather.service;

import com.wellnessview.gather.model.JobDefinition;
import com.wellnessview.gather.repository.JobDefinitionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;

@Service
public class GatherService {

    @Autowired
    private JobDefinitionRepository jobDefinitionRepository;

    @Autowired
    private DataSource dataSource;

    public JobDefinition save(JobDefinition jobDefinition) {
        // this must be idempotent
        JobDefinition existing = find(jobDefinition.getId());
        if (existing != null) {
            JobDefinition merged = new JobDefinition.Builder(jobDefinition, existing).build();
            return jobDefinitionRepository.save(merged);
        } else {
            return jobDefinitionRepository.save(jobDefinition);
        }
    }

    public JobDefinition find(Long id) {
        JobDefinition result = jobDefinitionRepository.findOne(id);
        return result;
    }

    public boolean delete(Long id) {
        jobDefinitionRepository.delete(id);

        return true;
    }
}
