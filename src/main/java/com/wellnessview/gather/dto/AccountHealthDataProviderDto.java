package com.wellnessview.gather.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountHealthDataProviderDto {

    Long id;

    private Long healthDataProviderId;

    String accountUid;

    private String accessToken;

    private String idToken;

    private String refreshToken;

    private String patientId;

    public Long getId() {
        return id;
    }

    public AccountHealthDataProviderDto setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getHealthDataProviderId() {
        return healthDataProviderId;
    }

    public AccountHealthDataProviderDto setHealthDataProviderId(Long healthDataProviderId) {
        this.healthDataProviderId = healthDataProviderId;
        return this;
    }

    public String getAccountUid() {
        return accountUid;
    }

    public AccountHealthDataProviderDto setAccountUid(String accountUid) {
        this.accountUid = accountUid;
        return this;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public AccountHealthDataProviderDto setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }

    public String getIdToken() {
        return idToken;
    }

    public AccountHealthDataProviderDto setIdToken(String idToken) {
        this.idToken = idToken;
        return this;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public AccountHealthDataProviderDto setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
        return this;
    }

    public String getPatientId() {
        return patientId;
    }

    public AccountHealthDataProviderDto setPatientId(String patientId) {
        this.patientId = patientId;
        return this;
    }
}
