package com.wellnessview.gather.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HealthDataProviderDto {

    private Long id;

    private String name;

    private String fhirResourceUrl;

    public Long getId() {
        return id;
    }

    public HealthDataProviderDto setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public HealthDataProviderDto setName(String name) {
        this.name = name;
        return this;
    }

    public String getFhirResourceUrl() {
        return fhirResourceUrl;
    }

    public HealthDataProviderDto setFhirResourceUrl(String fhirResourceUrl) {
        this.fhirResourceUrl = fhirResourceUrl;
        return this;
    }
}
