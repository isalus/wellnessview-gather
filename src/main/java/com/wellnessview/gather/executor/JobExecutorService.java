package com.wellnessview.gather.executor;

import com.wellnessview.gather.model.JobDefinition;
import com.wellnessview.gather.repository.JobDefinitionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

@Component
public class JobExecutorService {

    private static final Logger log = LoggerFactory.getLogger(JobExecutorService.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Autowired
    private JobDefinitionRepository jobDefinitionRepository;

    @Autowired
    private TaskExecutor taskExecutor;

    @Autowired JobExecutorFactory jobExecutorFactory;

    @Scheduled(fixedDelayString = "${wellnessview.gather.job.executor.fixedDelay}")
    public void runJobs() {
        log.info("The time is now {}", dateFormat.format(new Date()));
        findAndRunJobs();
    }

    private void findAndRunJobs() {
        // find all jobs that need to run
        Set<JobDefinition> jobsThatNeedToRun = jobDefinitionRepository.findByNeedsToRun(true);

        // run each job one at a time
        for (JobDefinition jobDefinition : jobsThatNeedToRun) {
            runJob(jobDefinition);
        }
    }

    private void runJob(JobDefinition jobDefinition) {
        // run the job
        jobExecutorFactory.createExecutor(jobDefinition).run();

        // todo need to block or somehow get notified when the job is complete
        // to mark it done

        // mark that the job doesn't need to run anymore
        JobDefinition jobThatHasRun =
                new JobDefinition.Builder(jobDefinition)
                        .setNeedsToRun(false)
                        .build();
        jobDefinitionRepository.save(jobThatHasRun);
    }

}
