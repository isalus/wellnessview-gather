package com.wellnessview.gather.executor;

import ca.uhn.fhir.context.FhirContext;
import com.wellnessview.gather.dto.AccountHealthDataProviderDto;
import com.wellnessview.gather.dto.HealthDataProviderDto;
import com.wellnessview.gather.executor.resourcetype.ObservationResourceTypeExecutor;
import com.wellnessview.gather.executor.resourcetype.PatientResourceTypeExecutor;
import com.wellnessview.gather.model.JobDefinition;
import com.wellnessview.gather.model.JobExecution;
import com.wellnessview.gather.repository.JobExecutionRepository;
import com.wellnessview.gather.repository.JobResourceChunkExecutionRepository;
import com.wellnessview.gather.repository.JobResourceExecutionRepository;
import org.hl7.fhir.dstu3.model.ResourceType;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.Set;

public class JobExecutor implements Runnable {

    private static final FhirContext FHIR_CONTEXT = FhirContext.forDstu3();

    private JobDefinition jobDefinition;

    private Set<ResourceType> resourcesToGather;

    private JobExecutionRepository jobExecutionRepository;

    private JobResourceExecutionRepository jobResourceExecutionRepository;

    private JobResourceChunkExecutionRepository jobResourceChunkExecutionRepository;

    private RestTemplate restTemplate;

    private String wellnessviewAccountUrl;

    public JobExecutor(JobDefinition jobDefinition, Set<ResourceType> resourcesToGather,
                       JobExecutionRepository jobExecutionRepository,
                       JobResourceExecutionRepository jobResourceExecutionRepository,
                       JobResourceChunkExecutionRepository jobResourceChunkExecutionRepository,
                       RestTemplate restTemplate,
                       String wellnessviewAccountUrl
    ) {
        this.jobDefinition = jobDefinition;
        this.resourcesToGather = resourcesToGather;
        this.jobExecutionRepository = jobExecutionRepository;
        this.jobResourceExecutionRepository = jobResourceExecutionRepository;
        this.jobResourceChunkExecutionRepository = jobResourceChunkExecutionRepository;
        this.restTemplate = restTemplate;
        this.wellnessviewAccountUrl = wellnessviewAccountUrl;
    }

    private AccountHealthDataProviderDto lookupAccountHealthDataProvider(JobDefinition jobDefinition) {
        // connect to target system and query for resources
        String url = wellnessviewAccountUrl
                + "/account/" + jobDefinition.getAccountId()
                + "/healthdataprovider/" + jobDefinition.getHealthDataProviderId();
        return restTemplate.getForObject(url, AccountHealthDataProviderDto.class);
    }

    private HealthDataProviderDto lookupHealthDataProvider(JobDefinition jobDefinition) {
        String url = wellnessviewAccountUrl
                + "/healthdataprovider/" + jobDefinition.getHealthDataProviderId();
        return restTemplate.getForObject(url, HealthDataProviderDto.class);
    }

    @Override
    public void run() {
        // create the JobExecution
        JobExecution jobExecution = new JobExecution.Builder()
                .setJobDefinition(jobDefinition)
                .setExecutionEndTime(new Date())
                .build();
        jobExecution = jobExecutionRepository.save(jobExecution);

        AccountHealthDataProviderDto accountHealthDataProviderDto = lookupAccountHealthDataProvider(jobDefinition);

        HealthDataProviderDto healthDataProviderDto = lookupHealthDataProvider(jobDefinition);

        // For each resource type
        for (ResourceType resourceType : resourcesToGather) {
            switch (resourceType) {
                // todo should these be on separate threads?
                case Patient:
                    // when should patient be synced? every single job definition execution?
                    new PatientResourceTypeExecutor(jobResourceExecutionRepository, jobResourceChunkExecutionRepository,
                            jobExecution, healthDataProviderDto, accountHealthDataProviderDto, restTemplate, FHIR_CONTEXT)
                            .run();
                    break;
                case Observation:
                    new ObservationResourceTypeExecutor(jobResourceExecutionRepository, jobResourceChunkExecutionRepository,
                            jobExecution, healthDataProviderDto, accountHealthDataProviderDto, restTemplate, FHIR_CONTEXT)
                            .run();
                    break;
            }
        }
    }

}
