package com.wellnessview.gather.executor.resourcetype;

import ca.uhn.fhir.context.FhirContext;
import com.wellnessview.gather.dto.AccountHealthDataProviderDto;
import com.wellnessview.gather.dto.HealthDataProviderDto;
import com.wellnessview.gather.model.JobExecution;
import com.wellnessview.gather.model.JobResourceExecution;
import com.wellnessview.gather.repository.JobResourceChunkExecutionRepository;
import com.wellnessview.gather.repository.JobResourceExecutionRepository;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.ResourceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

public class PatientResourceTypeExecutor extends ResourceTypeExecutor {

    private static final Logger log = LoggerFactory.getLogger(PatientResourceTypeExecutor.class);

    public PatientResourceTypeExecutor(JobResourceExecutionRepository jobResourceExecutionRepository,
                                       JobResourceChunkExecutionRepository jobResourceChunkExecutionRepository,
                                       JobExecution jobExecution,
                                       HealthDataProviderDto healthDataProviderDto,
                                       AccountHealthDataProviderDto accountHealthDataProviderDto,
                                       RestTemplate restTemplate,
                                       FhirContext fhirContext) {
        super(jobResourceExecutionRepository, jobResourceChunkExecutionRepository, jobExecution, healthDataProviderDto, accountHealthDataProviderDto, restTemplate, fhirContext);
    }

    @Override
    public void run() {
        log.info("Running sync for [Patient, " + accountHealthDataProviderDto.getAccountUid() + ", " + healthDataProviderDto.getName() + "]");

        // create the JobResourceExecution
        JobResourceExecution jobResourceExecution = new JobResourceExecution.Builder()
                .setJobExecution(jobExecution)
                .setExecutionStartTime(new Date())
                .setResourceType(ResourceType.Patient)
                .build();
        jobResourceExecution = jobResourceExecutionRepository.save(jobResourceExecution);

        // a patient should not use chunks or be bound to the start and end time frame of a job

        // using the access token
        ResponseEntity<String> response = buildAndExecuteFhirQuery("Patient/" + accountHealthDataProviderDto.getPatientId());

        Patient patient = (Patient) fhirContext.newJsonParser().parseResource(response.getBody());
        log.info("Found patient [" + patient.getId() + "]");

        // todo submit the patient to the consolidation service
        // with source system attribution

        // mark the resource as executed
        jobResourceExecution = new JobResourceExecution.Builder(jobResourceExecution)
                .setExecutionEndTime(new Date())
                .build();
        jobResourceExecutionRepository.save(jobResourceExecution);
        log.info("Completed sync for [Patient, " + accountHealthDataProviderDto.getAccountUid() + ", " + healthDataProviderDto.getName() + "]");
    }
}
