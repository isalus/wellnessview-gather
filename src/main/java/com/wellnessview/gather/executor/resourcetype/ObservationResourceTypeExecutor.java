package com.wellnessview.gather.executor.resourcetype;

import ca.uhn.fhir.context.FhirContext;
import com.wellnessview.gather.dto.AccountHealthDataProviderDto;
import com.wellnessview.gather.dto.HealthDataProviderDto;
import com.wellnessview.gather.model.JobDefinition;
import com.wellnessview.gather.model.JobExecution;
import com.wellnessview.gather.model.JobResourceChunkExecution;
import com.wellnessview.gather.model.JobResourceExecution;
import com.wellnessview.gather.repository.JobResourceChunkExecutionRepository;
import com.wellnessview.gather.repository.JobResourceExecutionRepository;
import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.Observation;
import org.hl7.fhir.dstu3.model.ResourceType;
import org.hl7.fhir.instance.model.api.IBaseBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.LinkedList;

public class ObservationResourceTypeExecutor extends ResourceTypeExecutor {

    private static final Logger log = LoggerFactory.getLogger(ObservationResourceTypeExecutor.class);

    private static int NUM_CHUNKS = 10;

    public ObservationResourceTypeExecutor(JobResourceExecutionRepository jobResourceExecutionRepository,
                                           JobResourceChunkExecutionRepository jobResourceChunkExecutionRepository,
                                           JobExecution jobExecution,
                                           HealthDataProviderDto healthDataProviderDto,
                                           AccountHealthDataProviderDto accountHealthDataProviderDto,
                                           RestTemplate restTemplate,
                                           FhirContext fhirContext) {
        super(jobResourceExecutionRepository, jobResourceChunkExecutionRepository, jobExecution, healthDataProviderDto, accountHealthDataProviderDto, restTemplate, fhirContext);
    }

    @Override
    public void run() {
        log.info("Running sync for [Observation, " + accountHealthDataProviderDto.getAccountUid() + ", " + healthDataProviderDto.getName() + "]");

        // create the JobResourceExecution
        JobResourceExecution jobResourceExecution = new JobResourceExecution.Builder()
                .setJobExecution(jobExecution)
                .setExecutionStartTime(new Date())
                .setResourceType(ResourceType.Observation)
                .build();
        jobResourceExecution = jobResourceExecutionRepository.save(jobResourceExecution);

        // create the chunks
        JobDefinition jobDefinition = jobExecution.getJobDefinition();
        long startInclusiveMillis = jobDefinition.getStartTimeInclusive().getTime();
        long endExclusiveMillis = jobDefinition.getEndTimeExclusive().getTime();
        long millisToGo = endExclusiveMillis - startInclusiveMillis - 1;
        long chunkSizeMillis = millisToGo / NUM_CHUNKS;
        long currentInclusiveMillis = startInclusiveMillis;
        LinkedList<JobResourceChunkExecution> chunkExecutions = new LinkedList<>();
        while (millisToGo > 0) {
            long thisChunkMillis = (millisToGo > chunkSizeMillis ? chunkSizeMillis : millisToGo);
            JobResourceChunkExecution jobResourceChunkExecution = new JobResourceChunkExecution.Builder()
                    .setJobResourceExecution(jobResourceExecution)
                    .setExecutionStartTime(new Date())
                    .setStartDateInclusive(new Date(currentInclusiveMillis))
                    .setEndDateExclusive(new Date(currentInclusiveMillis + thisChunkMillis))
                    .build();
            jobResourceChunkExecution = jobResourceChunkExecutionRepository.save(jobResourceChunkExecution);
            currentInclusiveMillis += thisChunkMillis;
            millisToGo -= thisChunkMillis;
            chunkExecutions.add(jobResourceChunkExecution);
        }

        // process the chunks
        for (JobResourceChunkExecution jobResourceChunkExecution : chunkExecutions) {
            // using the access token
            ResponseEntity<String> response = buildAndExecuteFhirQuery(
                    "Observation?patient=" + accountHealthDataProviderDto.getPatientId()
                            + "&_lastUpdated=ge" + DATE_FORMAT.format(jobResourceChunkExecution.getStartTimeInclusive())
                            + "&_lastUpdated=lt" + DATE_FORMAT.format(jobResourceChunkExecution.getEndTimeExclusive())
            );
            Bundle observationBundle = (Bundle) fhirContext.newJsonParser().parseResource(response.getBody());

            // paginated result
            boolean morePages = true;
            while (morePages) {
                // consume this page of data
                for (Bundle.BundleEntryComponent entry : observationBundle.getEntry()) {
                    Observation observation = (Observation) entry.getResource();
                    log.info("Found observation [" + observation.getId() + "]");

                    // todo submit the patient to the consolidation service
                    // with source system attribution
                }

                // mark the chunk as executed
                jobResourceChunkExecution = new JobResourceChunkExecution.Builder(jobResourceChunkExecution)
                        .setExecutionEndTime(new Date())
                        .build();
                jobResourceChunkExecutionRepository.save(jobResourceChunkExecution);

                // get the next page
                morePages = observationBundle.getLink(IBaseBundle.LINK_NEXT) != null;
                if (morePages) {
                    response = buildAndExecuteFhirQueryForUrl(observationBundle.getLink(IBaseBundle.LINK_NEXT).getUrl());
                    observationBundle = (Bundle) fhirContext.newJsonParser().parseResource(response.getBody());
                }
            }
        }

        // mark the resource as executed
        jobResourceExecution = new JobResourceExecution.Builder(jobResourceExecution)
                .setExecutionEndTime(new Date())
                .build();
        jobResourceExecutionRepository.save(jobResourceExecution);
        log.info("Completed sync for [Observation, " + accountHealthDataProviderDto.getAccountUid() + ", " + healthDataProviderDto.getName() + "]");
    }
}
