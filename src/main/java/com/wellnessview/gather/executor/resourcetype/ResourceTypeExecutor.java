package com.wellnessview.gather.executor.resourcetype;

import ca.uhn.fhir.context.FhirContext;
import com.wellnessview.gather.dto.AccountHealthDataProviderDto;
import com.wellnessview.gather.dto.HealthDataProviderDto;
import com.wellnessview.gather.model.JobExecution;
import com.wellnessview.gather.repository.JobResourceChunkExecutionRepository;
import com.wellnessview.gather.repository.JobResourceExecutionRepository;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;

abstract public class ResourceTypeExecutor implements Runnable {

    protected static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

    protected JobResourceExecutionRepository jobResourceExecutionRepository;

    protected JobResourceChunkExecutionRepository jobResourceChunkExecutionRepository;

    protected JobExecution jobExecution;

    protected HealthDataProviderDto healthDataProviderDto;

    protected AccountHealthDataProviderDto accountHealthDataProviderDto;

    protected RestTemplate restTemplate;

    protected FhirContext fhirContext;

    public ResourceTypeExecutor(JobResourceExecutionRepository jobResourceExecutionRepository,
                                       JobResourceChunkExecutionRepository jobResourceChunkExecutionRepository,
                                       JobExecution jobExecution,
                                       HealthDataProviderDto healthDataProviderDto,
                                       AccountHealthDataProviderDto accountHealthDataProviderDto,
                                       RestTemplate restTemplate,
                                       FhirContext fhirContext) {
        this.jobResourceExecutionRepository = jobResourceExecutionRepository;
        this.jobResourceChunkExecutionRepository = jobResourceChunkExecutionRepository;
        this.jobExecution = jobExecution;
        this.healthDataProviderDto = healthDataProviderDto;
        this.accountHealthDataProviderDto = accountHealthDataProviderDto;
        this.restTemplate = restTemplate;
        this.fhirContext = fhirContext;
    }

    protected ResponseEntity<String> buildAndExecuteFhirQuery(String query) {
        String url = healthDataProviderDto.getFhirResourceUrl() + "/" + query;
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + accountHealthDataProviderDto.getAccessToken());
        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
        return responseEntity;
    }

    protected ResponseEntity<String> buildAndExecuteFhirQueryForUrl(String fullQueryUrl) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + accountHealthDataProviderDto.getAccessToken());
        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(fullQueryUrl, HttpMethod.GET, request, String.class);
        return responseEntity;
    }

}
