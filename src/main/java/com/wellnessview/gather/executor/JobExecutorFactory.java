package com.wellnessview.gather.executor;

import com.wellnessview.gather.model.JobDefinition;
import com.wellnessview.gather.repository.JobExecutionRepository;
import com.wellnessview.gather.repository.JobResourceChunkExecutionRepository;
import com.wellnessview.gather.repository.JobResourceExecutionRepository;
import org.hl7.fhir.dstu3.model.ResourceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Set;

@Component
public class JobExecutorFactory {

    @Autowired
    private Set<ResourceType> resourcesToGather;

    @Autowired
    private JobExecutionRepository jobExecutionRepository;

    @Autowired
    private JobResourceExecutionRepository jobResourceExecutionRepository;

    @Autowired
    private JobResourceChunkExecutionRepository jobResourceChunkExecutionRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${wellnessview.account.url}")
    private String wellnessviewAccountUrl;

    public JobExecutor createExecutor(JobDefinition jobDefinition) {
        return new JobExecutor(jobDefinition, resourcesToGather, jobExecutionRepository,
                jobResourceExecutionRepository, jobResourceChunkExecutionRepository, restTemplate,
                wellnessviewAccountUrl);
    }

}
