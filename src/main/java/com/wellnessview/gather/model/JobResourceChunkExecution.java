package com.wellnessview.gather.model;

import org.apache.commons.lang3.Validate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class JobResourceChunkExecution implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private JobResourceExecution jobResourceExecution;

    @Column
    private Date executionStartTime;

    @Column
    private Date executionEndTime;

    @Column(nullable = false)
    private Date startTimeInclusive;

    @Column(nullable = false)
    private Date endTimeExclusive;

    JobResourceChunkExecution() {
    }

    public JobResourceChunkExecution(Long id, JobResourceExecution jobResourceExecution, Date executionStartTime, Date executionEndTime, Date startTimeInclusive, Date endTimeExclusive) {
        this.id = id;
        this.jobResourceExecution = jobResourceExecution;
        this.executionStartTime = executionStartTime;
        this.executionEndTime = executionEndTime;
        this.startTimeInclusive = startTimeInclusive;
        this.endTimeExclusive = endTimeExclusive;
    }

    public Long getId() {
        return id;
    }

    public JobResourceExecution getJobResourceExecution() {
        return jobResourceExecution;
    }

    public Date getExecutionStartTime() {
        return executionStartTime;
    }

    public Date getExecutionEndTime() {
        return executionEndTime;
    }

    public Date getStartTimeInclusive() {
        return startTimeInclusive;
    }

    public Date getEndTimeExclusive() {
        return endTimeExclusive;
    }

    public static final class Builder {
        Long id;
        JobResourceExecution jobResourceExecution;
        Date executionStartTime;
        Date executionEndTime;
        Date startDateInclusive;
        Date endDateExclusive;

        public Builder() {
        }

        public Builder(JobResourceChunkExecution jobResourceChunkExecution) {
            this.id = jobResourceChunkExecution.id;
            this.jobResourceExecution = jobResourceChunkExecution.jobResourceExecution;
            this.executionStartTime = jobResourceChunkExecution.executionStartTime;
            this.executionEndTime = jobResourceChunkExecution.executionEndTime;
            this.startDateInclusive = jobResourceChunkExecution.startTimeInclusive;
            this.endDateExclusive = jobResourceChunkExecution.endTimeExclusive;
        }

        /**
         * Merges source into target
         *
         * @param source All properties except id are retained
         * @param target ID is retained
         */
        public Builder(JobResourceChunkExecution source, JobResourceChunkExecution target) {
            if (target.getJobResourceExecution() != null) {
                Validate.isTrue(target.getJobResourceExecution().equals(source.getJobResourceExecution()), "Source and Target must have the same JobResourceExecution");
            }
            this.id = target.id;
            this.jobResourceExecution = source.jobResourceExecution;
            this.executionStartTime = source.executionStartTime;
            this.executionEndTime = source.executionEndTime;
            this.startDateInclusive = source.startTimeInclusive;
            this.endDateExclusive = source.endTimeExclusive;
        }

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public Builder setJobResourceExecution(JobResourceExecution jobResourceExecution) {
            this.jobResourceExecution = jobResourceExecution;
            return this;
        }

        public Builder setExecutionStartTime(Date executionStartTime) {
            this.executionStartTime = executionStartTime;
            return this;
        }

        public Builder setExecutionEndTime(Date executionEndTime) {
            this.executionEndTime = executionEndTime;
            return this;
        }

        public Builder setStartDateInclusive(Date startDateInclusive) {
            this.startDateInclusive = startDateInclusive;
            return this;
        }

        public Builder setEndDateExclusive(Date endDateExclusive) {
            this.endDateExclusive = endDateExclusive;
            return this;
        }

        public JobResourceChunkExecution build() {
            return new JobResourceChunkExecution(id, jobResourceExecution, executionStartTime, executionEndTime,
                    startDateInclusive, endDateExclusive);
        }
    }
}
