package com.wellnessview.gather.model;

import org.apache.commons.lang3.Validate;
import org.hl7.fhir.dstu3.model.ResourceType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class JobResourceExecution implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private JobExecution jobExecution;

    @Enumerated(EnumType.STRING)
    private ResourceType resourceType;

    @Column
    private Date executionStartTime;

    @Column
    private Date executionEndTime;

    JobResourceExecution() {
    }

    public JobResourceExecution(Long id, JobExecution jobExecution, ResourceType resourceType, Date executionStartTime, Date executionEndTime) {
        this.id = id;
        this.jobExecution = jobExecution;
        this.resourceType = resourceType;
        this.executionStartTime = executionStartTime;
        this.executionEndTime = executionEndTime;
    }

    public Long getId() {
        return id;
    }

    public ResourceType getResourceType() {
        return resourceType;
    }

    public JobExecution getJobExecution() {
        return jobExecution;
    }

    public Date getExecutionStartTime() {
        return executionStartTime;
    }

    public Date getExecutionEndTime() {
        return executionEndTime;
    }

    public static final class Builder {
        Long id;
        JobExecution jobExecution;
        ResourceType resourceType;
        Date executionStartTime;
        Date executionEndTime;

        public Builder() {
        }

        public Builder(JobResourceExecution jobResourceExecution) {
            this.id = jobResourceExecution.id;
            this.jobExecution = jobResourceExecution.jobExecution;
            this.resourceType = jobResourceExecution.resourceType;
            this.executionStartTime = jobResourceExecution.executionStartTime;
            this.executionEndTime = jobResourceExecution.executionEndTime;
        }

        /**
         * Merges source into target
         *
         * @param source All properties except id are retained
         * @param target ID is retained
         */
        public Builder(JobResourceExecution source, JobResourceExecution target) {
            if (target.getJobExecution() != null) {
                Validate.isTrue(target.getJobExecution().equals(source.getJobExecution()), "Source and Target must have the same JobExecution");
            }
            this.id = target.id;
            this.jobExecution = source.jobExecution;
            this.resourceType = source.resourceType;
            this.executionStartTime = source.executionStartTime;
            this.executionEndTime = source.executionEndTime;
        }

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public Builder setJobExecution(JobExecution jobExecution) {
            this.jobExecution = jobExecution;
            return this;
        }

        public Builder setResourceType(ResourceType resourceType) {
            this.resourceType = resourceType;
            return this;
        }

        public Builder setExecutionStartTime(Date executionStartTime) {
            this.executionStartTime = executionStartTime;
            return this;
        }

        public Builder setExecutionEndTime(Date executionEndTime) {
            this.executionEndTime = executionEndTime;
            return this;
        }

        public JobResourceExecution build() {
            return new JobResourceExecution(id, jobExecution, resourceType, executionStartTime, executionEndTime);
        }
    }
}
