package com.wellnessview.gather.model;

import org.apache.commons.lang3.Validate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class JobExecution implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private JobDefinition jobDefinition;

    @Column
    private Date executionStartTime;

    @Column
    private Date executionEndTime;

    JobExecution() {
    }

    public JobExecution(Long id, JobDefinition jobDefinition, Date executionStartTime, Date executionEndTime) {
        this.id = id;
        this.jobDefinition = jobDefinition;
        this.executionStartTime = executionStartTime;
        this.executionEndTime = executionEndTime;
    }

    public Long getId() {
        return id;
    }

    public JobDefinition getJobDefinition() {
        return jobDefinition;
    }

    public Date getExecutionStartTime() {
        return executionStartTime;
    }

    public Date getExecutionEndTime() {
        return executionEndTime;
    }

    public static final class Builder {
        Long id;
        JobDefinition jobDefinition;
        Date executionStartTime;
        Date executionEndTime;

        public Builder() {
        }

        public Builder(JobExecution jobExecution) {
            this.id = jobExecution.id;
            this.jobDefinition = jobExecution.jobDefinition;
            this.executionStartTime = jobExecution.executionStartTime;
            this.executionEndTime = jobExecution.executionEndTime;
        }

        /**
         * Merges source into target
         *
         * @param source All properties except id are retained
         * @param target ID is retained
         */
        public Builder(JobExecution source, JobExecution target) {
            if (target.getJobDefinition() != null) {
                Validate.isTrue(target.getJobDefinition().equals(source.getJobDefinition()), "Source and Target must have the same JobDefinition");
            }
            this.id = target.id;
            this.jobDefinition = source.jobDefinition;
            this.executionStartTime = source.executionStartTime;
            this.executionEndTime = source.executionEndTime;
        }

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public Builder setJobDefinition(JobDefinition jobDefinition) {
            this.jobDefinition = jobDefinition;
            return this;
        }

        public Builder setExecutionStartTime(Date executionStartTime) {
            this.executionStartTime = executionStartTime;
            return this;
        }

        public Builder setExecutionEndTime(Date executionEndTime) {
            this.executionEndTime = executionEndTime;
            return this;
        }

        public JobExecution build() {
            return new JobExecution(id, jobDefinition, executionStartTime, executionEndTime);
        }
    }
}
