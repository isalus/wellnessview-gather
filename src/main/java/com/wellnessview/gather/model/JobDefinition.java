package com.wellnessview.gather.model;

import org.apache.commons.lang3.Validate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class JobDefinition implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String accountId;

    @Column(nullable = false)
    private Long healthDataProviderId;

    @Column(nullable = false)
    private Boolean enabled;

    @Column(nullable = false)
    private Boolean needsToRun = true;

    @Column(nullable = false)
    private Date startTimeInclusive;

    @Column(nullable = false)
    private Date endTimeExclusive;

    JobDefinition() {
    }

    private JobDefinition(Long id, String accountId, Long healthDataProviderId, Boolean enabled, Boolean needsToRun, Date startTimeInclusive, Date endTimeExclusive) {
        this.id = id;
        this.accountId = accountId;
        this.healthDataProviderId = healthDataProviderId;
        this.enabled = enabled;
        this.needsToRun = needsToRun;
        this.startTimeInclusive = startTimeInclusive;
        this.endTimeExclusive = endTimeExclusive;
    }

    public Long getId() {
        return id;
    }

    public String getAccountId() {
        return accountId;
    }

    public Long getHealthDataProviderId() {
        return healthDataProviderId;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public Boolean getNeedsToRun() {
        return needsToRun;
    }

    public Date getStartTimeInclusive() {
        return startTimeInclusive;
    }

    public Date getEndTimeExclusive() {
        return endTimeExclusive;
    }

    public static final class Builder {
        Long id;
        String accountId;
        Long healthDataProviderId;
        Boolean enabled;
        Boolean needsToRun;
        Date startDateInclusive;
        Date endDateExclusive;

        public Builder() {
        }

        public Builder(JobDefinition jobDefinition) {
            this.id = jobDefinition.id;
            this.accountId = jobDefinition.accountId;
            this.healthDataProviderId = jobDefinition.healthDataProviderId;
            this.enabled = jobDefinition.enabled;
            this.needsToRun = jobDefinition.needsToRun;
            this.startDateInclusive = jobDefinition.startTimeInclusive;
            this.endDateExclusive = jobDefinition.endTimeExclusive;
        }

        /**
         * Merges source into target
         *
         * @param source All properties except id are retained
         * @param target ID is retained
         */
        public Builder(JobDefinition source, JobDefinition target) {
            if (target.getAccountId() != null) {
                Validate.isTrue(target.getAccountId().equals(source.getAccountId()),
                        "Source and Target must have the same AccountId");
            }
            if (target.getHealthDataProviderId() != null) {
                Validate.isTrue(target.getHealthDataProviderId().equals(source.getHealthDataProviderId()),
                        "Source and Target must have the same HealthDataProviderId");
            }
            this.id = target.id;
            this.enabled = source.enabled;
            this.needsToRun = source.needsToRun;
            this.startDateInclusive = source.startTimeInclusive;
            this.endDateExclusive = source.endTimeExclusive;
        }

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public Builder setAccountId(String accountId) {
            this.accountId = accountId;
            return this;
        }

        public Builder setHealthDataProviderId(Long healthDataProviderId) {
            this.healthDataProviderId = healthDataProviderId;
            return this;
        }

        public Builder setEnabled(Boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        public Builder setNeedsToRun(Boolean needsToRun) {
            this.needsToRun = needsToRun;
            return this;
        }

        public Builder setStartDateInclusive(Date startDateInclusive) {
            this.startDateInclusive = startDateInclusive;
            return this;
        }

        public Builder setEndDateExclusive(Date endDateExclusive) {
            this.endDateExclusive = endDateExclusive;
            return this;
        }

        public JobDefinition build() {
            return new JobDefinition(id, accountId, healthDataProviderId, enabled, needsToRun, startDateInclusive, endDateExclusive);
        }
    }
}
