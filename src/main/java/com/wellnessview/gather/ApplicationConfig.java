package com.wellnessview.gather;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.hl7.fhir.dstu3.model.ResourceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;

import java.util.HashSet;
import java.util.Set;

@Configuration
public class ApplicationConfig {

    @Autowired
    @Primary
    public void configDataSource(DataSource dataSource) {
        dataSource.setTestOnBorrow(true);
        dataSource.setValidationQuery("SELECT 1");
    }

    @Bean
    public TaskExecutor jobExecutor() {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        // todo externalize this
        threadPoolTaskExecutor.setMaxPoolSize(10);
        return threadPoolTaskExecutor;
    }

    @Bean
    public Set<ResourceType> resourcesToGather() {
        Set<ResourceType> supportedResourceTypes = new HashSet<ResourceType>();
        supportedResourceTypes.add(ResourceType.Patient);
        supportedResourceTypes.add(ResourceType.Observation);
        return supportedResourceTypes;
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }
}
