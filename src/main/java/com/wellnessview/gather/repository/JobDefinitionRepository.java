package com.wellnessview.gather.repository;

import com.wellnessview.gather.model.JobDefinition;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public interface JobDefinitionRepository extends CrudRepository<JobDefinition, Long> {

    Set<JobDefinition> findByNeedsToRun(Boolean needsToRun);

}
