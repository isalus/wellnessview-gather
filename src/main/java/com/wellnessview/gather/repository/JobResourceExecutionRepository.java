package com.wellnessview.gather.repository;

import com.wellnessview.gather.model.JobResourceExecution;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface JobResourceExecutionRepository extends CrudRepository<JobResourceExecution, Long> {

//    Set<JobExecution> findByJobExecutionId(Long jobExecutionId);

}
