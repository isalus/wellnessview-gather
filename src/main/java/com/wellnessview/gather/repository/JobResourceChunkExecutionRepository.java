package com.wellnessview.gather.repository;

import com.wellnessview.gather.model.JobResourceChunkExecution;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface JobResourceChunkExecutionRepository extends CrudRepository<JobResourceChunkExecution, Long> {


}
