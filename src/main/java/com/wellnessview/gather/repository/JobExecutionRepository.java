package com.wellnessview.gather.repository;

import com.wellnessview.gather.model.JobExecution;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface JobExecutionRepository extends CrudRepository<JobExecution, Long> {

//    Set<JobExecution> findByJobDefinitionId(Long jobDefinitionId);

}
