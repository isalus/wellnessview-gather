create table job_execution (
    id bigint not null auto_increment,
    execution_end_time datetime,
    execution_start_time datetime,
    job_definition_id bigint not null,
    primary key (id)
);

alter table job_execution
add constraint fk_job_definition
foreign key (job_definition_id)
references job_definition (id);