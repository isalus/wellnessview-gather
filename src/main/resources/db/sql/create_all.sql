CREATE DATABASE wellnessview_gather;

USE wellnessview_gather;

SOURCE job_definition.sql;

SOURCE job_execution.sql;

SOURCE job_resource_execution.sql;

SOURCE job_resource_chunk_execution.sql;