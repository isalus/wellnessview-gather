create table job_resource_execution (
    id bigint not null auto_increment,
    resource_type varchar(255) not null,
    execution_end_time datetime,
    execution_start_time datetime,
    job_execution_id bigint not null,
    primary key (id)
);

alter table job_resource_execution
add constraint fk_job_execution
foreign key (job_execution_id)
references job_execution (id);