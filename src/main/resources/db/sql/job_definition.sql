create table job_definition (
    id bigint not null auto_increment,
    account_id varchar(255) not null,
    health_data_provider_id bigint not null,
    enabled bit not null,
    needs_to_run bit not null,
    end_time_exclusive datetime not null,
    start_time_inclusive datetime not null,
    primary key (id)
);