create table job_resource_chunk_execution (
    id bigint not null auto_increment,
    execution_start_time datetime,
    execution_end_time datetime,
    job_resource_execution_id bigint not null,
    start_time_inclusive datetime not null,
    end_time_exclusive datetime not null,
    primary key (id)
);

alter table job_resource_chunk_execution
add constraint fk_job_resource_execution
foreign key (job_resource_execution_id)
references job_resource_execution (id);