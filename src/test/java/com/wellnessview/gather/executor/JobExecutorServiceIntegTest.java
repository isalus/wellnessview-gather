package com.wellnessview.gather.executor;

import com.wellnessview.gather.model.JobDefinition;
import com.wellnessview.gather.repository.JobDefinitionRepository;
import com.wellnessview.gather.repository.JobExecutionRepository;
import com.wellnessview.gather.repository.JobResourceChunkExecutionRepository;
import com.wellnessview.gather.repository.JobResourceExecutionRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringBootTest
public class JobExecutorServiceIntegTest {

    @Autowired
    JobExecutorService jobExecutorService;

    @Autowired
    private JobDefinitionRepository jobDefinitionRepository;

    @Autowired
    private JobExecutionRepository jobExecutionRepository;

    @Autowired
    private JobResourceExecutionRepository jobResourceExecutionRepository;

    @Autowired
    private JobResourceChunkExecutionRepository jobResourceChunkExecutionRepository;

    private String uid = "xuYltIToD0T2GWV6JPuqXxpwfgi1";

    private Long healthDataProviderId = 1L;

    private JobDefinition jobDefinition;

    @Before
    public void setUp() {
        Date start = new Date(1999, 0, 1);
        Date end = new Date(2000, 0, 1);
        JobDefinition jobDefinition = new JobDefinition.Builder()
                .setAccountId(uid)
                .setHealthDataProviderId(healthDataProviderId)
                .setStartDateInclusive(start)
                .setEndDateExclusive(end)
                .setEnabled(true)
                .setNeedsToRun(true)
                .build();
        jobDefinition = jobDefinitionRepository.save(jobDefinition);
    }

    @After
    public void tearDown() {
        jobResourceChunkExecutionRepository.deleteAll();
        jobResourceExecutionRepository.deleteAll();
        jobExecutionRepository.deleteAll();
        jobDefinitionRepository.deleteAll();
    }

    @Test
    public void runTest() {
        jobExecutorService.runJobs();
    }

}