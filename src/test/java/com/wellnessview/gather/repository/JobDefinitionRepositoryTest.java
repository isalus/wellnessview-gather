package com.wellnessview.gather.repository;

import com.wellnessview.gather.model.JobDefinition;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class JobDefinitionRepositoryTest {

    @Autowired
    private JobDefinitionRepository jobDefinitionRepository;

    @Test
    public void testCRUD() throws Exception {
        String mockAccountId = "AccountId";
        Long mockHealthDataProviderId = 1L;
        Date start = new Date();
        Date end = new Date(start.getTime() + 100);
        JobDefinition jobDefinition = new JobDefinition.Builder()
                .setAccountId(mockAccountId)
                .setHealthDataProviderId(mockHealthDataProviderId)
                .setStartDateInclusive(start)
                .setEndDateExclusive(end)
                .setEnabled(true)
                .setNeedsToRun(true)
                .build();
        JobDefinition saved = jobDefinitionRepository.save(jobDefinition);
        assertNotNull(saved);
        assertNotNull(saved.getId());
        assertEquals(jobDefinition.getAccountId(), saved.getAccountId());
        assertEquals(jobDefinition.getHealthDataProviderId(), saved.getHealthDataProviderId());
        assertEquals(jobDefinition.getStartTimeInclusive(), saved.getStartTimeInclusive());
        assertEquals(jobDefinition.getEndTimeExclusive(), saved.getEndTimeExclusive());
        assertEquals(jobDefinition.getEnabled(), saved.getEnabled());
        assertEquals(jobDefinition.getNeedsToRun(), saved.getNeedsToRun());

        Long id = saved.getId();

        JobDefinition found = jobDefinitionRepository.findOne(id);
        assertNotNull(found);

        Date secondStart = new Date(start.getTime() + 1000);
        Date secondEnd = new Date(end.getTime() + 1000);
        JobDefinition updated = new JobDefinition.Builder(found)
                .setStartDateInclusive(secondStart)
                .setEndDateExclusive(secondEnd)
                .build();

        saved = jobDefinitionRepository.save(updated);
        assertNotNull(saved);
        assertNotNull(saved.getId());
        assertEquals(jobDefinition.getId(), saved.getId());
        assertEquals(jobDefinition.getAccountId(), saved.getAccountId());
        assertEquals(jobDefinition.getHealthDataProviderId(), saved.getHealthDataProviderId());
        assertEquals(secondStart, saved.getStartTimeInclusive());
        assertEquals(secondEnd, saved.getEndTimeExclusive());
        assertEquals(saved.getEnabled(), saved.getEnabled());
        assertEquals(saved.getNeedsToRun(), saved.getNeedsToRun());

        jobDefinitionRepository.delete(saved);

        found = jobDefinitionRepository.findOne(id);
        assertNull(found);
    }
}