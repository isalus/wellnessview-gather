package com.wellnessview.gather.repository;

import com.wellnessview.gather.model.JobDefinition;
import com.wellnessview.gather.model.JobExecution;
import com.wellnessview.gather.model.JobResourceChunkExecution;
import com.wellnessview.gather.model.JobResourceExecution;
import org.hl7.fhir.dstu3.model.ResourceType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class JobResourceChunkExecutionRepositoryTest {

    @Autowired
    private JobDefinitionRepository jobDefinitionRepository;

    @Autowired
    private JobExecutionRepository jobExecutionRepository;

    @Autowired
    private JobResourceExecutionRepository jobResourceExecutionRepository;

    @Autowired
    private JobResourceChunkExecutionRepository jobResourceChunkExecutionRepository;

    @Test
    public void testCRUD() throws Exception {
        String mockAccountId = "AccountId";
        Long mockHealthDataProviderId = 1L;
        Date start = new Date();
        Date end = new Date(start.getTime() + 100);
        JobDefinition jobDefinition = new JobDefinition.Builder()
                .setAccountId(mockAccountId)
                .setHealthDataProviderId(mockHealthDataProviderId)
                .setStartDateInclusive(start)
                .setEndDateExclusive(end)
                .setEnabled(true)
                .setNeedsToRun(true)
                .build();
        JobDefinition savedDefinition = jobDefinitionRepository.save(jobDefinition);

        JobExecution jobExecution = new JobExecution.Builder()
                .setJobDefinition(savedDefinition)
                .setExecutionStartTime(start)
                .setExecutionEndTime(end)
                .build();
        JobExecution savedExecution = jobExecutionRepository.save(jobExecution);

        JobResourceExecution jobResourceExecution = new JobResourceExecution.Builder()
                .setJobExecution(savedExecution)
                .setResourceType(ResourceType.Account)
                .setExecutionStartTime(start)
                .setExecutionEndTime(end)
                .build();
        JobResourceExecution savedJobResourceExecution = jobResourceExecutionRepository.save(jobResourceExecution);

        JobResourceChunkExecution jobResourceChunkExecution = new JobResourceChunkExecution.Builder()
                .setJobResourceExecution(jobResourceExecution)
                .setExecutionStartTime(start)
                .setExecutionEndTime(end)
                .build();
        JobResourceChunkExecution saved = jobResourceChunkExecutionRepository.save(jobResourceChunkExecution);

        assertNotNull(saved);
        assertNotNull(saved.getId());
        assertEquals(savedJobResourceExecution.getId(), saved.getJobResourceExecution().getId());
        assertEquals(start, saved.getExecutionStartTime());
        assertEquals(end, saved.getExecutionEndTime());

        Long id = saved.getId();

        JobResourceChunkExecution found = jobResourceChunkExecutionRepository.findOne(id);
        assertNotNull(found);

        Date secondStart = new Date(start.getTime() + 1000);
        Date secondEnd = new Date(end.getTime() + 1000);
        JobResourceChunkExecution updated = new JobResourceChunkExecution.Builder(found)
                .setExecutionStartTime(secondStart)
                .setExecutionEndTime(secondEnd)
                .build();

        saved = jobResourceChunkExecutionRepository.save(updated);
        assertNotNull(saved);
        assertEquals(updated.getId(), saved.getId());
        assertEquals(secondStart, saved.getExecutionStartTime());
        assertEquals(secondEnd, saved.getExecutionEndTime());

        jobResourceChunkExecutionRepository.delete(saved);

        found = jobResourceChunkExecutionRepository.findOne(id);
        assertNull(found);

        jobResourceExecutionRepository.delete(jobResourceExecution);
        jobExecutionRepository.delete(jobExecution);
        jobDefinitionRepository.delete(jobDefinition);
    }
}