package com.wellnessview.gather.repository;

import com.wellnessview.gather.model.JobDefinition;
import com.wellnessview.gather.model.JobExecution;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class JobExecutionRepositoryTest {

    @Autowired
    private JobDefinitionRepository jobDefinitionRepository;

    @Autowired
    private JobExecutionRepository jobExecutionRepository;

    @Test
    public void testCRUD() throws Exception {
        String mockAccountId = "AccountId";
        Long mockHealthDataProviderId = 1L;
        Date start = new Date();
        Date end = new Date(start.getTime() + 100);
        JobDefinition jobDefinition = new JobDefinition.Builder()
                .setAccountId(mockAccountId)
                .setHealthDataProviderId(mockHealthDataProviderId)
                .setStartDateInclusive(start)
                .setEndDateExclusive(end)
                .setEnabled(true)
                .setNeedsToRun(true)
                .build();
        JobDefinition savedDefinition = jobDefinitionRepository.save(jobDefinition);
        assertNotNull(savedDefinition);
        assertNotNull(savedDefinition.getId());

        JobExecution jobExecution = new JobExecution.Builder()
                .setJobDefinition(jobDefinition)
                .setExecutionStartTime(start)
                .setExecutionEndTime(end)
                .build();
        JobExecution saved = jobExecutionRepository.save(jobExecution);

        Long id = saved.getId();

        JobExecution found = jobExecutionRepository.findOne(id);
        assertNotNull(found);

        Date secondStart = new Date(start.getTime() + 1000);
        Date secondEnd = new Date(end.getTime() + 1000);
        JobExecution updated = new JobExecution.Builder(found)
                .setExecutionStartTime(secondStart)
                .setExecutionEndTime(secondEnd)
                .build();

        saved = jobExecutionRepository.save(updated);
        assertNotNull(saved);
        assertNotNull(saved.getId());
        assertEquals(jobExecution.getId(), saved.getId());
        assertEquals(jobExecution.getJobDefinition().getId(), saved.getJobDefinition().getId());
        assertEquals(secondStart, saved.getExecutionStartTime());
        assertEquals(secondEnd, saved.getExecutionEndTime());

        jobExecutionRepository.delete(saved);

        found = jobExecutionRepository.findOne(id);
        assertNull(found);

        jobDefinitionRepository.delete(jobDefinition);
    }
}