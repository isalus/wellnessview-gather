package com.wellnessview.gather.repository;

import com.wellnessview.gather.model.JobDefinition;
import com.wellnessview.gather.model.JobExecution;
import com.wellnessview.gather.model.JobResourceExecution;
import org.hl7.fhir.dstu3.model.ResourceType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
//@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class JobResourceExecutionRepositoryTest {

    @Autowired
    private JobDefinitionRepository jobDefinitionRepository;

    @Autowired
    private JobExecutionRepository jobExecutionRepository;

    @Autowired
    private JobResourceExecutionRepository jobResourceExecutionRepository;

    @Test
    public void testCRUD() throws Exception {
        String mockAccountId = "AccountId";
        Long mockHealthDataProviderId = 1L;
        Date start = new Date();
        Date end = new Date(start.getTime() + 100);
        JobDefinition jobDefinition = new JobDefinition.Builder()
                .setAccountId(mockAccountId)
                .setHealthDataProviderId(mockHealthDataProviderId)
                .setStartDateInclusive(start)
                .setEndDateExclusive(end)
                .setEnabled(true)
                .setNeedsToRun(true)
                .build();
        JobDefinition savedDefinition = jobDefinitionRepository.save(jobDefinition);

        JobExecution jobExecution = new JobExecution.Builder()
                .setJobDefinition(savedDefinition)
                .setExecutionStartTime(start)
                .setExecutionEndTime(end)
                .build();
        JobExecution savedExecution = jobExecutionRepository.save(jobExecution);

        JobResourceExecution jobResourceExecution = new JobResourceExecution.Builder()
                .setJobExecution(savedExecution)
                .setResourceType(ResourceType.Account)
                .setExecutionStartTime(start)
                .setExecutionEndTime(end)
                .build();
        JobResourceExecution saved = jobResourceExecutionRepository.save(jobResourceExecution);
        assertNotNull(saved);
        assertNotNull(saved.getId());
        assertEquals(savedExecution.getId(), saved.getJobExecution().getId());
        assertEquals(ResourceType.Account, saved.getResourceType());
        assertEquals(start, saved.getExecutionStartTime());
        assertEquals(end, saved.getExecutionEndTime());

        Long id = saved.getId();

        JobResourceExecution found = jobResourceExecutionRepository.findOne(id);
        assertNotNull(found);

        Date secondStart = new Date(start.getTime() + 1000);
        Date secondEnd = new Date(end.getTime() + 1000);
        JobResourceExecution updated = new JobResourceExecution.Builder(found)
                .setResourceType(ResourceType.ActivityDefinition)
                .setExecutionStartTime(secondStart)
                .setExecutionEndTime(secondEnd)
                .build();

        saved = jobResourceExecutionRepository.save(updated);
        assertNotNull(saved);
        assertEquals(updated.getId(), saved.getId());
        assertEquals(ResourceType.ActivityDefinition, saved.getResourceType());
        assertEquals(secondStart, saved.getExecutionStartTime());
        assertEquals(secondEnd, saved.getExecutionEndTime());

        jobResourceExecutionRepository.delete(saved);

        found = jobResourceExecutionRepository.findOne(id);
        assertNull(found);

        jobExecutionRepository.delete(jobExecution);
        jobDefinitionRepository.delete(jobDefinition);
    }
}