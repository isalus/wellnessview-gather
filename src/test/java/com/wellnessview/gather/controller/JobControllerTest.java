//package com.wellnessview.gather.controller;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.wellnessview.gather.Application;
//import com.wellnessview.gather.model.JobDefinition;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.SpringApplicationConfiguration;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.web.WebAppConfiguration;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.context.WebApplicationContext;
//
//import java.util.Date;
//
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = Application.class)
//@WebAppConfiguration
//public class JobControllerTest {
//
//    @Autowired
//    private WebApplicationContext ctx;
//
//    @Autowired
//    private ObjectMapper objectMapper;
//
//    private MockMvc mockMvc;
//
//    @Before
//    public void setUp() {
//        this.mockMvc = MockMvcBuilders.webAppContextSetup(ctx).build();
//    }
//
//    @Test
//    public void testHealth() throws Exception {
//        this.mockMvc
//                .perform(get("/gather/health"))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(content().string("OK"));
//    }
//
//    @Test
//    public void testCreateDelete() throws Exception {
//        Long mockAccountHealthDataProviderId = 1L;
//
//        Date start = new Date();
//        Date end = new Date(start.getTime() + 100);
//        JobDefinition jobDefinition =
//                new JobDefinition.Builder()
//                        .setAccountHealthDataProviderId(mockAccountHealthDataProviderId)
//                        .setStartDateInclusive(start)
//                        .setEndDateExclusive(end)
//                        .setEnabled(true)
//                        .build();
//
//        this.mockMvc
//                .perform(post("/gather").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsBytes(jobDefinition)))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(content().string("hi"));
//
////        this.mockMvc
////                .perform(delete("/gather/" + id))
////                .andExpect(status().isOk());
//    }
//
////    @Test
////    public void testIdempotency() throws Exception {
////        UUID id = UUID.randomUUID();
////        String accountUid = "accountUid";
////        Date startFirst = new Date();
////        Date endFirst = new Date(startFirst.getTime() + 100);
////        Date startSecond = new Date(startFirst.getTime() + 1000);
////        Date endSecond = new Date(startFirst.getTime() + 1100);
////        JobDefinition first = new JobDefinition.Builder()
////                .withId(id)
////                .withAccountUid(accountUid)
////                .withPeriodStartDate(startFirst)
////                .withPeriodEndDate(endFirst)
////                .build();
////        JobDefinition second = new JobDefinition.Builder()
////                .withId(id)
////                .withAccountUid(accountUid)
////                .withPeriodStartDate(startSecond)
////                .withPeriodEndDate(endSecond)
////                .build();
////
////        this.mockMvc
////                .perform(put("/gather/" + id).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsBytes(first)))
////                .andExpect(status().isOk());
////
////        this.mockMvc
////                .perform(get("/gather/" + id))
////                .andExpect(status().isOk());
////
////        // to test idempotency, submit with the same UID (business key) but no id
////        // to verify the same PUT can be repeated
////        this.mockMvc
////                .perform(put("/gather/" + id).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsBytes(second)))
////                .andExpect(status().isOk());
////
////        this.mockMvc
////                .perform(get("/gather/" + id))
////                .andExpect(status().isOk());
////
////        this.mockMvc
////                .perform(delete("/gather/" + id))
////                .andExpect(status().isOk());
////    }
////
////    @Test
////    public void testGet() throws Exception {
////        UUID id = UUID.randomUUID();
////        String accountUid = "accountUid";
////        Date start = new Date();
////        Date end = new Date(start.getTime() + 100);
////
////        // this is failing, possibly need to URL encode the id first
////        this.mockMvc
////                .perform(get("/gather/" + id))
////                .andExpect(status().isNotFound());
////
////        JobDefinition jobDefinition = new JobDefinition.Builder()
////                .withId(id)
////                .withAccountUid(accountUid)
////                .withPeriodStartDate(start)
////                .withPeriodEndDate(end)
////                .build();
////
////        this.mockMvc
////                .perform(put("/gather/" + id)
////                        .contentType(MediaType.APPLICATION_JSON)
////                        .content(objectMapper.writeValueAsBytes(jobDefinition)))
////                .andExpect(status().isOk());
////
////        this.mockMvc
////                .perform(get("/gather/" + id))
////                .andExpect(status().isOk());
////
////        this.mockMvc
////                .perform(delete("/gather/" + id))
////                .andExpect(status().isOk());
////    }
//}